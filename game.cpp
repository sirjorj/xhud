#include "colors.h"
#include "printhelpers.h"
#include "game.h"
#include "imagegen.h"
#include "helpers.h"
#include <cctype>
#include <iostream>
#include <regex>
#include <stdio.h>



Game::Game(std::array<Squad, 2>& p, SettingsManager &s)
  : token{s, p[0], p[1]}, players(p), isRunning(true) {
}

void Game::Run() {
  GenerateImage(token);//this->players[0], this->players[1], this->outFile);
  do {
    std::string line;
    printf("%sxhud> ", CLR_GPT);
    std::getline(std::cin, line);
    if(this->ParseCommand(line)) {
      GenerateImage(token);//this->players[0], this->players[1], this->outFile);
    }
  } while(this->isRunning);
}

// https://regex101.com
// remember: ignore the R"()"
// options
const std::regex RX_QUIT(R"(^qqq$)");                                   // quit
const std::regex RX_HELP(R"(^[?]$)");                                   // help
// data lookup
const std::regex RX_PI(R"(^p[ ]+[a-z0-9-]+([ ]+[a-z0-9]+[ ]+[a-z0-9-]+)?$)"); // pilot info
const std::regex RX_UI(R"(^u[ ]+[a-z0-9]+([ ]+[a-z0-9]+)?$)");                // upgrade info
// settings
const std::regex RX_SETP(R"(^settings$)");                                                 // print all current settings
const std::regex RX_SETR(R"(^settings reload( [[:alnum:].]+)?$)");                         // reload setting(s) from config
const std::regex RX_SETW(R"(^settings write$)");                                           // write setting(s) to config
const std::regex RX_SETS(R"(^settings [[:alnum:].]+(=[[:alnum:][:space:][:punct:]]*)?$)"); // set key (or print value if no 'val')
// game info
const std::regex RX_INIT(R"(^[i][012]$)");                              // initiative
// squads
const std::regex RX_PLT( R"(^[12][1-8][SsHhAa]+$)");                    // pilot
const std::regex RX_UPG( R"(^[12][1-8][1-9]([AaXxFf]+|\([a-z]*\))+$)"); // upgrade
const std::regex RX_OVR( R"(^[12][1-8]((O(sk|at|ag|hu|sh)[0-9]{1,2})|(o(sk|at|ag|hu|sh)))$)"); // override
// popups
const std::regex RX_CLR( R"(^[Cc]$)");                // clear popups
const std::regex RX_ATTD(R"(^[12][Aa][CcHhFfBb]*$)"); // attack dice
const std::regex RX_DEFD(R"(^[12][Dd][EeFfBb]*$)");   // defense dice
const std::regex RX_FOCD(R"(^[12][Ff]$)");            // focus dice
//const std::regex RX_PLTC(R"()");  // pilot card
//const std::regex RX_UPGC(R"()");  // upgrade card



// return is whether or not to redraw the images
bool Game::ParseCommand(std::string command) {

  if(command == "") {
    printf("%s  Title: '%s'\n", CLR_GAM, this->token.settings.GetSetting("run.title").c_str());
    for(uint8_t player : {1,2}) {
      printf("%s  Player %hhu%s%s\n", CLR_GAM, player, CLR_LBL, this->token.initiative==player ? " [INITIATIVE]" : "");
      uint8_t pc = 1;
      uint8_t nameLen = 0;
      for(auto p : this->players[player-1].GetPilots()) {
	if(p.GetName().length() > nameLen) { nameLen = p.GetName().length(); }
	for(auto u : p.GetAppliedUpgrades()) {
	  if(u.GetName().length()+2 > nameLen) { nameLen = u.GetName().length()+2; }
	}
      }
      for(auto p : this->players[player-1].GetPilots()) {
	printf("    %s%hhu%s) %s%-*s", CLR_PLT, pc++, CLR_LBL, CLR_PLT, nameLen, p.GetName().c_str());
	printf(" %s[%s%s%s]", CLR_GAM,  p.IsEnabled() ? CLR_ENA : CLR_DIS, p.IsEnabled() ? "A" : "a", CLR_GAM);
	printf(" %s%2d%s", CLR_SKL, p.GetModSkill(), CLR_GAM);
	printf(" %s%2d%s", CLR_ATK, p.GetModAttack(), CLR_GAM);
	printf(" %s%2d%s", CLR_AGI, p.GetModAgility(), CLR_GAM);
	printf(" %s%2d%s", CLR_HUL, p.GetModHull(), CLR_GAM);
	printf(" %s%2d%s", CLR_SHD, p.GetModShield(), CLR_GAM);
	printf(" [");
	for(int i=0; i<p.GetCurHull();    i++) { printf("%sH", CLR_HUL); }
	for(int i=0; i<p.GetHullHits();   i++) { printf("%s.", CLR_HUL); }
	for(int i=0; i<p.GetCurShield();  i++) { printf("%sS", CLR_SHD); }
	for(int i=0; i<p.GetShieldHits(); i++) { printf("%s.", CLR_SHD); }
	printf("%s]", CLR_GAM);
	printf("\n");
	uint8_t uc = 1;
	for(auto u : p.GetAppliedUpgrades()) {
	  printf("      %s%hhu%s) %s%-*s", CLR_UPG, uc++, CLR_LBL, CLR_UPG, nameLen-2, u.GetName().c_str());
	  printf(" %s[%s%s%s]", CLR_GAM, u.IsEnabled() ? CLR_ENA : CLR_DIS, u.IsEnabled() ? "A" : "a", CLR_GAM);
	  printf(" %s", CLR_ENA);
	  for(int i=0; i<u.GetExtras(); i++) {
	    printf("X");
	  }
	  printf("%s\n", CLR_GAM);
	}
      }
    }
    printf("\n");
  }

  for(auto cmd : tokenize(command, ',')) {

    if(std::regex_match(cmd, RX_QUIT)) {
      printf("%sQuit!\n", CLR_GAM);
      this->isRunning = false;
    }

    // help
    else if(std::regex_match(cmd, RX_HELP)) {
      printf("%s", CLR_GAM);
      printf("Commands:\n");
      printf("[Options]\n");
      printf("  ?      - help\n");
      printf("  qqq    - quit\n");
      printf("         - print current status (yes, that's a blank line)\n");
      printf("[Settings]\n");
      printf("  settings         - print the current settings\n");
      printf("  settings {k}={v} - set setting {k} to {v}\n");
      printf("  settings write   - write the current settings to the config file\n");
      printf("[Game State]\n");
      printf("  i#     - set player '#' as having initiative (0 to disable)\n");
      printf("  <PSC>  - modify ship stats\n");
      printf("  <PSUC> - modify upgrade status\n");
      printf("   P     - player number (1 or 2)\n");
      printf("   S     - ship number (1..n counting down)\n");
      printf("   U     - upgrade number (1..n left to right, top to bottom)\n");
      printf("   C     - command(s)\n");
      printf("     s     - shield down\n");
      printf("     S     - shield up\n");
      printf("     h     - hull down\n");
      printf("     H     - hull up\n");
      printf("     a     - inactive\n");
      printf("     A     - active\n");
      printf("     x     - remove Extra token (upgrade only)\n");
      printf("     X     - add Extra token (upgrade only)\n");
      printf("     f     - flip card (upgrade only)\n");
      printf("     F     - (same as 'f')\n");
      printf("     oxx   - disable override (where 'xx' is sk, at, ag, hu, sh)\n");
      printf("     Oxxv  - overrides 'xx' (where 'xx' is sk, at, ag, hu, sh) to value 'v'\n");
      printf("[Dice]\n");
      printf("  <PDR>   - Print dice results where:\n");
      printf("   P - player (1 or 2))\n");
      printf("   D - die type ('a' for attack, 'd' for defense)\n");
      printf("   R - 1 or more results where:\n");
      printf("     c - critical hit\n");
      printf("     h - hit\n");
      printf("     f - focus\n");
      printf("     b - blank\n");
      printf("     e - evade\n");
      printf("  c  - clear the dice results\n");
      printf("[Data Lookup]\n");
      printf("  p n     - search for pilot 'n' and show its info\n");
      printf("  p n f s - show info for pilot specified by name/faction/ship\n");
      printf("  u n     - search for upgrade 'n' and show its info\n");
      printf("  u t n   - show info for upgrade specified by upgradetype/upgradename\n");
      printf("\n");
      printf("Examples:\n");
      printf("    i1       - player 1 has initiative\n");
      printf("    13s      - player 1, ship 3 loses a shield\n");
      printf("    21ssh    - player 2, ship 1 loses 2 shield and a hull\n");
      printf("    11hha    - player 1, ship 1 loses 2 hull and is disabled\n");
      printf("    231a     - player 2, ship 3, upgrade 1 is disabled\n");
      printf("    11h, 12h - player 1, ships 1 and 2 each lose a hull\n");
      printf("    1ahhf    - player 1 rolls 2 hits and a focus\n");
      printf("    2debb    - player 2 rolls an evade and 2 blanks\n");
      printf("    c        - clear the dice results\n");
      printf("    settings run.title=My Game - Sets title to \"My Game\"\n");
    }

    // get pilot info
    else if(std::regex_match(cmd, RX_PI)) {
      std::string target = cmd.substr(2);
      std::vector<std::string> tokens = tokenize(target, ' ');
      switch(tokens.size()) {
      case 1:
	{
	  std::vector<Pilot> ps = Pilot::FindPilot(tokens[0]);
	  if     (ps.size() == 0) { printf("  Pilot not found\n"); }
	  else if(ps.size() == 1) { PrintPilot(ps[0]); }
	  else                    { PrintPilots(ps); }
	}
	break;
      case 3:
	{
	  try{
	    Pilot p = Pilot::GetPilot(tokens[0], tokens[1], tokens[2]);
	    PrintPilot(p);
	  }
	  catch(PilotNotFound e) {
	    printf("Pilot not found: %s\n", e.what());
	  }
	}
	break;
      default:
	printf("How did this happen?\n");
      }
    }

    // get upgrade info
    else if(std::regex_match(cmd, RX_UI)) {
      std::string target = cmd.substr(2);
      std::vector<std::string> tokens = tokenize(target, ' ');
      switch(tokens.size()) {
      case 1:
	{
	  std::vector<Upgrade> us = Upgrade::FindUpgrade(tokens[0]);
	  if     (us.size() == 0) { printf("  Upgrade not found\n"); }
	  else if(us.size() == 1) { PrintUpgrade(us[0]); }
	  else                    { PrintUpgrades(us); }
	}
	break;
      case 2:
	{
	  try{
	    Upgrade u = Upgrade::GetUpgrade(tokens[0], tokens[1]);
	    PrintUpgrade(u);
	  }
	  catch(UpgradeNotFound e) {
	    printf("Upgrade not found: %s\n", e.what());
	  }
	}
	break;
      default:
	printf("How did this happen?\n");
      }
    }

    // print all settings
    else if(std::regex_match(cmd, RX_SETP)) {
      token.settings.Dump();
    }

    // reload settings
    //else if(std::regex_match(cmd, RX_SETR)) {
    //  printf("Reload Settings not yet implemented\n");
    //}

    // write settings
    else if(std::regex_match(cmd, RX_SETW)) {
      token.settings.WriteSettings();
      printf("Settings written to config\n");
    }

    // set a setting
    else if(std::regex_match(cmd, RX_SETS)) {
      std::string str = cmd.substr(9); // skip "settings "
      std::string key = str.substr(0,str.find('='));
      std::experimental::optional<std::string> val;
      if(str.find('=') != std::string::npos) {
	val = str.substr(str.find('=')+1);
      }
      //printf("str -> %s\n", str.c_str());
      //printf("key -> %s\n", key.c_str());
      //printf("val -> %s\n", val ? val->c_str() : "*null*");
      if(val) {
	token.settings.SetSetting(key, *val);
      }
      else {
	try {
	  val = token.settings.GetSetting(key);
	}
	catch(SettingNotFound &snf) {}
	if(val) {
	  printf("  %s%s %s-> %s%s\n", CLR_KEY, key.c_str(), CLR_DEF, CLR_VAL, val->c_str());
	} else {
	  printf("  %sInvalid key %s%s\n", CLR_ERR, CLR_KEY, key.c_str());
	}
      }
    }

    // initiative
    else if(std::regex_match(cmd, RX_INIT)) {
      uint8_t init = cmd[1]-48;
      this->token.initiative = init;
      if(init == 0) {
        printf("%s  Removing Initiative\n", CLR_GAM);
      } else {
        printf("%s  Setting Initiative to player %hhu\n", CLR_GAM, init);
      }
    }

    // clear
    else if(std::regex_match(cmd, RX_CLR)) {
      this->token.dice[0].state = Dice::State::None;
      this->token.dice[0].attackDice = AttackDice();
      this->token.dice[0].defenseDice = DefenseDice();
      this->token.dice[1].state = Dice::State::None;
      this->token.dice[1].attackDice = AttackDice();
      this->token.dice[1].defenseDice = DefenseDice();
      printf("%s  Clearing popups\n", CLR_GAM);
    }

    // attack dice
    else if(std::regex_match(cmd, RX_ATTD)) {
      uint8_t player = cmd[0]-48;
      AttackDice ad;
      for(auto a : cmd.substr(2)) {
        switch(a) {
        case 'B': case 'b': ad.Add(AttD::Blank); break;
        case 'F': case 'f': ad.Add(AttD::Focus); break;
        case 'H': case 'h': ad.Add(AttD::Hit);   break;
        case 'C': case 'c': ad.Add(AttD::Crit);  break;
        }
      }
      this->token.dice[player-1].attackDice = ad;
      this->token.dice[player-1].state = ad.GetDice().size() ? Dice::State::Attacking : Dice::State::None;
      if(ad.GetDice().size()) {
        printf("%s  Setting attack dice: ", CLR_GAM);
        for(auto a : ad.GetSortedDice()) {
          printf("[%s%s%s]", CLR_ATK, a.GetShortName().c_str(), CLR_GAM);
        }
        printf("\n");
      } else {
        printf("%s  Clearing attack dice\n", CLR_GAM);
      }
    }

    // defense dice
    else if(std::regex_match(cmd, RX_DEFD)) {
      uint8_t player = cmd[0]-48;
      DefenseDice ed;
      for(auto e : cmd.substr(2)) {
        switch(e) {\
        case 'B': case 'b': ed.Add(DefD::Blank); break;
        case 'F': case 'f': ed.Add(DefD::Focus); break;
        case 'E': case 'e': ed.Add(DefD::Evade); break;
        }
      }
      this->token.dice[player-1].defenseDice = ed;
      this->token.dice[player-1].state = ed.GetDice().size() ? Dice::State::Defending : Dice::State::None;
      if(ed.GetDice().size()) {
        printf("%s  Setting evade dice: ", CLR_GAM);
        for(auto a : ed.GetSortedDice()) {
          printf("[%s%s%s]", CLR_AGI, a.GetShortName().c_str(), CLR_GAM);
        }
        printf("\n");
      } else {
        printf("%s  Clearing defense dice\n", CLR_GAM);
      }
    }

    // focus dice
    else if(std::regex_match(cmd, RX_FOCD)) {
      uint8_t player = cmd[0]-48;
      printf("focusing results for player %d\n",  player);
      switch(this->token.dice[player-1].state) {
      case Dice::State::Attacking: this->token.dice[player-1].attackDice.Focus(); break;
      case Dice::State::Defending: this->token.dice[player-1].defenseDice.Focus(); break;
      default: break;
      }
    }

    // pilot
    else if(std::regex_match(cmd, RX_PLT)) {
      uint8_t player = cmd[0] - 48; // verified by regex
      uint8_t ship   = cmd[1] - 48; // needs bounds checking
      if((ship < 1) || ( ship > this->players[player-1].GetPilots().size())) {
        printf("%sInvalid Ship\n", CLR_ERR);
        continue;
      }
      std::string pn = this->players[player-1].GetPilots()[ship-1].GetName();
      for(auto c : cmd.substr(2)) {
        switch(c) {
        case 's': this->players[player-1].GetPilots()[ship-1].ShieldDn(); printf("%s  Player %d - Pilot %d (%s%s%s) - Shield Down\n", CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
        case 'S': this->players[player-1].GetPilots()[ship-1].ShieldUp(); printf("%s  Player %d - Pilot %d (%s%s%s) - Shield Up\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
        case 'h': this->players[player-1].GetPilots()[ship-1].HullDn();   printf("%s  Player %d - Pilot %d (%s%s%s) - Hull Down\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM);
	  if(token.settings.GetSetting("run.autodisable") == "yes") {
	    if(this->players[player-1].GetPilots()[ship-1].GetCurHull() == 0) {
	      this->players[player-1].GetPilots()[ship-1].Disable();
	      printf("%s  Player %d - Pilot %d (%s%s%s) - Inactive\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM);
	    }
	  }
	  break;
        case 'H': this->players[player-1].GetPilots()[ship-1].HullUp();   printf("%s  Player %d - Pilot %d (%s%s%s) - Hull Up\n",     CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
        case 'a': this->players[player-1].GetPilots()[ship-1].Disable();  printf("%s  Player %d - Pilot %d (%s%s%s) - Inactive\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
        case 'A': this->players[player-1].GetPilots()[ship-1].Enable();   printf("%s  Player %d - Pilot %d (%s%s%s) - Active\n",      CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); break;
        }
      }
    }

    // override
    else if(std::regex_match(cmd, RX_OVR)) {
      uint8_t player = cmd[0] - 48; // verified by regex
      uint8_t ship   = cmd[1] - 48; // needs bounds checking
      if((ship < 1) || ( ship > this->players[player-1].GetPilots().size())) {
        printf("%sInvalid Ship\n", CLR_ERR);
        continue;
      }
      std::string pn = this->players[player-1].GetPilots()[ship-1].GetName();
      std::string target;
      target += cmd[3];
      target += cmd[4];
      switch(cmd[2]) {
      case 'o':
	{
	  if(target == "sk") { this->players[player-1].GetPilots()[ship-1].ClearOverrideSkill();   printf("%s  Player %d - Pilot %d (%s%s%s) - Remove override on Skill\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); }
	  if(target == "at") { this->players[player-1].GetPilots()[ship-1].ClearOverrideAttack();  printf("%s  Player %d - Pilot %d (%s%s%s) - Remove override on Attack\n",  CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); }
	  if(target == "ag") { this->players[player-1].GetPilots()[ship-1].ClearOverrideAgility(); printf("%s  Player %d - Pilot %d (%s%s%s) - Remove override on Agility\n", CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); }
	  if(target == "hu") { this->players[player-1].GetPilots()[ship-1].ClearOverrideHull();    printf("%s  Player %d - Pilot %d (%s%s%s) - Remove override on Hull\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); }
	  if(target == "sh") { this->players[player-1].GetPilots()[ship-1].ClearOverrideShield();  printf("%s  Player %d - Pilot %d (%s%s%s) - Remove override on Shield\n",  CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM); }
	}
	break;
      case 'O':
	{
	  uint8_t val = std::stoi(cmd.substr(5));
	  if(target == "sk") { this->players[player-1].GetPilots()[ship-1].OverrideSkill(val);   printf("%s  Player %d - Pilot %d (%s%s%s) - Override Skill to %d\n",   CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM, val); }
	  if(target == "at") { this->players[player-1].GetPilots()[ship-1].OverrideAttack(val);  printf("%s  Player %d - Pilot %d (%s%s%s) - Override Attack to %d\n",  CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM, val); }
	  if(target == "ag") { this->players[player-1].GetPilots()[ship-1].OverrideAgility(val); printf("%s  Player %d - Pilot %d (%s%s%s) - Override Agility to %d\n", CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM, val); }
	  if(target == "hu") { this->players[player-1].GetPilots()[ship-1].OverrideHull(val);    printf("%s  Player %d - Pilot %d (%s%s%s) - Override Hull to %d\n",    CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM, val); }
	  if(target == "sh") { this->players[player-1].GetPilots()[ship-1].OverrideShield(val);  printf("%s  Player %d - Pilot %d (%s%s%s) - Override Shield to %d\n",  CLR_GAM, player, ship, CLR_PLT, pn.c_str(), CLR_GAM, val); }
	}
	break;
      }
    }

    // upgrade
    else if(std::regex_match(cmd, RX_UPG)) {
      uint8_t player = cmd[0] - 48; // verified by regex
      uint8_t ship   = cmd[1] - 48; // needs bounds checking
      uint8_t upg    = cmd[2] - 48; // needs bounds checking
      if(ship > this->players[player-1].GetPilots().size()) {
        printf("%sInvalid Ship\n", CLR_ERR);
        continue;
      }
      if((upg < 1) || (upg > this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades().size()+1)) {
        printf("%sInvalid upgrade\n", CLR_ERR);
        continue;
      }
      std::string pn = this->players[player-1].GetPilots()[ship-1].GetName();
      std::string un = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GetName();
      for(int i=2; i<=cmd.size(); i++) {
        switch(cmd[i]) {
        case 'a':
          this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].Disable();
          printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Inactive\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
          break;
        case 'A':
          this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].Enable();
          printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Active\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
          break;
        case 'x':
          this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].ExtraDn();
          printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Removed Extra\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
          break;
        case 'X':
          this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].ExtraUp();
          printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Added Extra\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
          break;
        case 'f':
        case 'F':
          this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].Flip();
          // upgrade values changed, so refresh our local copy
          un = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1].GetName();
          printf("%s  Player %d - Pilot %d (%s%s%s) - Upgrade %d (%s%s%s) - Flipped\n", CLR_GAM, player, ship, CLR_SHP, pn.c_str(), CLR_GAM, upg, CLR_GAM, un.c_str(), CLR_GAM);
          break;
        case '(':
          std::string newUpg;
          i++;
          while(cmd[i] != ')') {
            newUpg += cmd[i++];
          }
          if(newUpg == "") {
            Upgrade oldUp = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1];
            printf("%sPossible replacements:\n", CLR_GAM);
            for(Upgrade u : Upgrade::GetAllUpgrades()) {
              if(u.GetType().GetType() == oldUp.GetType().GetType()) {
                printf("%s  %s\n", CLR_UPG, u.GetXws().c_str());
              }
            }
          } else {
            Upgrade oldUp = this->players[player-1].GetPilots()[ship-1].GetAppliedUpgrades()[upg-1];
            try{
              this->players[player-1].GetPilots()[ship-1].ReplaceUpgrade(upg-1, oldUp.GetType().GetXws(), newUpg);
            }
            catch(...) {
              printf("%s'%s' is invalid\n", CLR_ERR, newUpg.c_str());
              return false;
            }
          }
          break;
        }
      }
    }

    // invalid command
    else {
      printf("%sInvalid command '%s' - enter '?' for help\n", CLR_ERR, cmd.c_str());
    }
  }

  return true;
}
