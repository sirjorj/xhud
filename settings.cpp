#include "settings.h"
#include "colors.h"
#include <algorithm>
#include <fstream>
#include <regex>



static std::string trimString(std::string str) {
  if(str.length() == 0) return str;
  size_t first = str.find_first_not_of(' ');
  size_t last = str.find_last_not_of(' ');
  return str.substr(first, (last-first+1));
}



// *** SettingType ***
SettingTypeNotFound::SettingTypeNotFound(STp s) : runtime_error("SettingType not found: (enum " + std::to_string((int)s) + ")") { }


std::vector<SettingType> SettingType::settingTypes = {
  { STp::Bool, "Bool", std::regex(R"(^(yes|no)$)"),                       "Boolean ('yes' or 'no')" },
  { STp::Path, "Path", std::regex(R"([^\0]*)"),                           "A path to a file or directory" },
  { STp::Text, "Text", std::regex(R"(^[[:alnum:][:space:][:punct:]]*$)"), "Any text" },
};


SettingType SettingType::GetSettingType(STp typ) {
  for(SettingType s : SettingType::settingTypes) {
    if(s.GetType() == typ) {
      return s;
    }
  }
  throw SettingTypeNotFound(typ);
}

STp         SettingType::GetType()  { return this->type; }
std::string SettingType::GetName()  { return this->name; }
std::regex  SettingType::GetRegex() { return this->regex; }
std::string SettingType::GetDesc()  { return this->desc; }

SettingType::SettingType(STp         t,
			 std::string n,
			 std::regex  r,
			 std::string d)
  : type(t), name(n), regex(r), desc(d) { }



// *** SettingsManager ***
SettingNotFound::SettingNotFound(std::string s) : runtime_error(s) {};
InvalidSetting::InvalidSetting(std::string k, std::string v) : runtime_error("'" + k + "' => '" + v + "'") {};

SettingsManager::SettingsManager(std::string file)
  : filename(file)
  , settingsRecords({
      // key               exp        def   desc
      { "run.autodisable", STp::Bool, "no", "Automatically disable a pilot when hull reaches zero" },
      { "run.multiimage",  STp::Bool, "no", "Generate separate images for each player or a single one for the whole screen" },
      { "run.outdir",      STp::Path, "",   "Default output file for 'run' mode" },
      { "run.title",       STp::Text, "",   "Default title for the overlay"},
    })
{
  this->VerifySettings(false);
  // load settings into memory
  for(SettingsRecord sr : this->settingsRecords) {
    std::experimental::optional<std::string> fromConfig = this->GetSettingFromConfig(sr.key);
    this->SetSetting(sr.key, fromConfig ? *fromConfig : sr.defaultVal);
  }
}



std::vector<std::string> SettingsManager::GetAllKeys() const {
  std::vector<std::string> keys;
  for(auto setting : this->settingsRecords) {
    keys.push_back(setting.key);
  }
  return keys;
}



#define VERBOSE1(s, ...) if(verbose)printf(s, ##__VA_ARGS__)
#define VERBOSE2(s, ...) if(verbose)printf("  line %2d: " s, lineNumber, ##__VA_ARGS__)
void SettingsManager::VerifySettings(bool verbose) const {
  std::ifstream settingsStream(this->filename.c_str());
  int lineNumber = 0;
  VERBOSE1("Config File:\n");
  for(std::string line; getline(settingsStream, line); ) {
    lineNumber++;

    std::string k, v;
    std::string l = trimString(line);
    if(l.length() == 0) {
      continue;
    }
    else if(l[0] == '#') {
      continue;
    }

    int split = l.find("=");
    if(split == std::string::npos) {
      VERBOSE2("ERROR - no '='\n");
    } else {
      k = trimString(l.substr(0, split));
      v = trimString(l.substr(split+1));
      SettingsRecord newSetting;
      std::experimental::optional<SettingsRecord> sr = SettingsManager::GetSettingRecord(k);
      if(sr) {
        if(std::regex_match(v, SettingType::GetSettingType(sr->type).GetRegex())) {
	  VERBOSE2("'%s' => '%s'\n", k.c_str(), v.c_str());
	} else {
	  VERBOSE2("ERROR - invalid value '%s' for key '%s'\n", v.c_str(), k.c_str()); 
	}
      } else {
        VERBOSE2("ERROR - unknown setting '%s'\n", k.c_str());
      }
    }
  }
  settingsStream.close();

  if(verbose) {
    printf("Settings:\n");
    int keyLen=0;
    for(SettingsRecord sr : this->settingsRecords) {
      if(sr.key.length() > keyLen) { keyLen = sr.key.length(); }
    }
    
    for(SettingsRecord sr : this->settingsRecords) {
      std::string value = this->GetSetting(sr.key);
      printf("  %-*s => %s\n", keyLen, sr.key.c_str(), value.c_str());
    }
  }
}



std::string SettingsManager::GetSetting(std::string key) const {
  if(this->settingsData.find(key) == this->settingsData.end()) {
    throw SettingNotFound(key);
  }
  return this->settingsData.at(key);
}



std::experimental::optional<std::string> SettingsManager::GetSettingFromConfig(std::string key) const {
  // find setting in SettingRecord
  std::experimental::optional<SettingsRecord> sr = this->GetSettingRecord(key);
  if(!sr) { throw SettingNotFound(key); }

  // now we have the info - see if the setting is set
  std::ifstream settingsStream(this->filename.c_str());
  for(std::string line; getline(settingsStream, line); ) {
    std::string l = trimString(line);
    if     (l.length() == 0)   { continue; }
    else if(l[0]       == '#') { continue; }
    int split = l.find("=");
    if(split != std::string::npos) {
      std::string k = trimString(l.substr(0, split));
      std::string v = trimString(l.substr(split+1));
      if(k == key) {
	return v;
      }
    }
  }
  settingsStream.close();

  return std::experimental::nullopt;
}



void SettingsManager::SetSetting(std::string key, std::string value) {
  std::experimental::optional<SettingsRecord> sr = this->GetSettingRecord(key);
  if(!sr) { throw InvalidSetting(key, value); }
  
  if(std::regex_match(value, SettingType::GetSettingType(sr->type).GetRegex())) {
    this->settingsData[key] = value;
  } else {
    throw InvalidSetting(key, value);
  }
}



void SettingsManager::WriteSettings() {
  std::ofstream ofs;
  ofs.open(this->filename, std::ofstream::out | std::ofstream::trunc);

  for(SettingsRecord sr : this->settingsRecords) {
    std::string key = sr.key;
    std::string def = sr.defaultVal;
    std::string val = this->settingsData[key];
    if(val != def) {
      ofs << key << "=" << val << std::endl;
    }
  }

  ofs.close();
}



void SettingsManager::Dump() {
  struct data {
    std::string key;
    std::string def;
    std::experimental::optional<std::string> config;
    std::string setting;
  };
  std::vector<data> table;
  int kLen = 3;
  int dLen = 3;
  int cLen = 6;
  int sLen = 7;
  for(SettingsRecord sr : this->settingsRecords) {
    data d;

    d.key = sr.key;
    kLen = std::max(kLen, (int)d.key.length());

    d.def = sr.defaultVal;
    dLen = std::max(dLen, (int)d.def.length());
    
    d.config = this->GetSettingFromConfig(d.key);
    cLen = std::max(cLen, d.config ? (int)d.config->length() : 0);

    d.setting = this->GetSetting(d.key);
    sLen = std::max(sLen, (int)d.setting.length());

    table.push_back(d);
  }

  printf("%s%-*s   %-*s   %-*s   %-*s\n", CLR_LBL, kLen, "Key", dLen, "Def", cLen, "Config", sLen, "Setting");
  for(data d : table) {
    printf("%s%-*s   %s%-*s   %s%-*s   %s%-*s%s\n",
	   CLR_KEY, kLen, d.key.c_str(),
	   CLR_VAL, dLen, d.def.c_str(),
	   CLR_VAL, cLen, d.config ? d.config->c_str() : "",
	   ((d.config && (*d.config == d.setting)) || (d.setting == d.def)) ? CLR_VAL : CLR_SHP, sLen, d.setting.c_str(),
	   CLR_DEF);
  }
}



std::experimental::optional<SettingsRecord> SettingsManager::GetSettingRecord(std::string s) const {
  for(auto setting : this->settingsRecords) {
    if(setting.key == s) {
      return setting;
    }
  }
  return {};
}
